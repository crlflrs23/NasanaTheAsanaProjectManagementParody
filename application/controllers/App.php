<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

	function __construct(){
	parent::__construct();
		$this->load->model('Task_Model');
		$this->load->model('User_Model');
		$this->load->library('parser');
		date_default_timezone_set('Asia/Manila');
	}

	public function index() {
		$session = $this->session->userdata('admin_logged_in');
		if(!$session) {
			redirect(base_url());
		}

		if(isset($_REQUEST['failed'])) {
			$fail = $_REQUEST['failed'];
			switch($fail) {
				case 'add_task':
					$data['error_message'] = "Error adding Task. Please click Add task again.";
				break;
			}
		}

		$data['info'] = $session;
		$data['users'] = $this->User_Model->get_all_users();
		$data['sections'] = $this->Task_Model->get_all_sections();
		$data['assigned'] = $this->Task_Model->get_user_task($session['user_id']);
		$data['page_title'] = "nasana - " . $session['display_name'];
		$this->parser->parse('header_view', $data);
		$this->load->view('app_view', $data);
		$this->load->view('footer_view', $data);
	}

	public function add_task(){
		$session = $this->session->userdata('admin_logged_in');

		$this->form_validation->set_rules('task_title', 'Task Title ', 'required|xss_clean');
		$this->form_validation->set_rules('task_description', 'Task Description ', 'xss_clean');
		$this->form_validation->set_rules('task_priority', 'Task Priority ', 'required|xss_clean');
		$this->form_validation->set_rules('task_section', 'Task Section ', 'required|xss_clean');
		$this->form_validation->set_rules('task_assign', 'Task Assigned ', 'required|xss_clean');

		if($this->form_validation->run()) {
			$check = $this->Task_Model->check_section($this->input->post('task_section'));
			if($check->count <= 0) {
				$section_data = array(
					"section_name" => $this->input->post('task_section'),
					"user_id" => $session['user_id'],
					"date_created" => date('Y-m-d H:i:s', time())
				);
				$section_id = $this->Task_Model->add_section($section_data);
			} else {
				$result = $this->Task_Model->get_section_data($this->input->post('task_section'));
				$section_id = $result->section_id;
			}

			$user = $this->User_Model->get_user_by_username($this->input->post('task_assign'));
			$task_data = array(
				"section_id" => $section_id,
				"task_title" => $this->input->post('task_title'),
				"task_desc" => $this->input->post('task_description'),
				"task_priority" => $this->input->post('task_priority'),
				"task_added" => $session['user_id'],
				"task_assign" => $user->user_id,
				"date_created" => date('Y-m-d H:i:s', time()),
				"date_modified" => date('Y-m-d H:i:s', time()),
				"task_done" => date('Y-m-d H:i:s', time())
			);

			$checker = $this->Task_Model->add_task($task_data);

			if($checker >= 1) {
				redirect(base_url() . 'app/?success=add_task&'. $check);
			} else {
				redirect(base_url() . 'app/?failed=add_task');
			}
		} else {
			redirect(base_url() . 'app/?failed=add_task');
		}
	}

	public function task_information($task_id) {
		if(! $this->input->is_ajax_request()) {
		    redirect('404');
		}

		$data['task'] = $this->Task_Model->get_task_data($task_id);
		$this->load->view('task_info_view', $data);
	}

	public function edit_task_info($task_id) {
		$session = $this->session->userdata('admin_logged_in');

		$this->form_validation->set_rules('task_title', 'Task Title ', 'required|xss_clean');
		$this->form_validation->set_rules('task_description', 'Task Description ', 'xss_clean');
		$this->form_validation->set_rules('task_priority', 'Task Priority ', 'required|xss_clean');
		$this->form_validation->set_rules('task_section', 'Task Section ', 'required|xss_clean');
		$this->form_validation->set_rules('task_assign', 'Task Assigned ', 'required|xss_clean');

		if($this->form_validation->run()) {
			$check = $this->Task_Model->check_section($this->input->post('task_section'));
			if($check->count <= 0) {
				$section_data = array(
					"section_name" => $this->input->post('task_section'),
					"user_id" => $session['user_id'],
					"date_created" => date('Y-m-d H:i:s', time())
				);
				$section_id = $this->Task_Model->add_section($section_data);
			} else {
				$result = $this->Task_Model->get_section_data($this->input->post('task_section'));
				$section_id = $result->section_id;
			}

			$user = $this->User_Model->get_user_by_username($this->input->post('task_assign'));
			$task_data = array(
				"section_id" => $section_id,
				"task_title" => $this->input->post('task_title'),
				"task_desc" => $this->input->post('task_description'),
				"task_priority" => $this->input->post('task_priority'),
				"task_assign" => $user->user_id,
				"date_modified" => date('Y-m-d H:i:s', time())
			);

			$checker = $this->Task_Model->update_task($task_data, $task_id);
			redirect(base_url().'app/?success=edit_task#/'.$task_id);
			exit();
		}

		$data['users'] = $this->User_Model->get_all_users();
		$data['sections'] = $this->Task_Model->get_all_sections();
		$data['task'] = $this->Task_Model->get_task_data($task_id);
		$this->load->view('edit_task_info_view', $data);
	}

	public function update_task($task_id) {
		if(! $this->input->is_ajax_request()) {
		    redirect('404');
		}



		$check = $this->Task_Model->get_task_data($task_id)->task_status;
		if($check == 1) {
			$data = array(
				"task_status" => 0,
				"task_done" => date('Y-m-d H:i:s', time()),
				"date_modified" =>  date('Y-m-d H:i:s', time())
			);

			if($this->Task_Model->update_task_status($task_id, $data) >= 1) {
				echo "true/push";
			} else {
				echo "false";
			}
		} else {
			$data = array(
				"task_status" => 1,
				"task_done" => date('Y-m-d H:i:s', time()),
				"date_modified" =>  date('Y-m-d H:i:s', time())
			);

			if($this->Task_Model->update_task_status($task_id, $data) >= 1) {
				echo "true/revoke";
			} else {
				echo "false";
			}
		}
	}

	public function delete_task($task_id) {
		$this->Task_Model->delete_task($task_id);
		echo "true";
	}

	public function test() {
		echo $_SERVER['HTTP_REFERER'];
	}

	public function update_section($id) {
		$val = $this->input->post('c');
		$data = array(
			"section_name" => $val
			);

		$this->Task_Model->update_section($data, $id);
	}

	public function delete_section($id) {
		$this->Task_Model->delete_task_section($id);
		$this->Task_Model->delete_section($id);
	}

	public function get_progress() {
		if(!$this->input->is_ajax_request()) {
		    echo "The hell shits!";
			exit();
		}

		$chart = $this->Task_Model->get_chart_data();

		$remain = '';
		$done = '';
		$labels = "";

		$td = 0;
		$tr = 0;
		foreach ($chart as $key => $value) {
		    $remain .=  $value->remaining . ",";
		    $done .=  $value->done . ",";
		    $labels .= "'" . $value->date . "',";

		    $td += $value->done;
		    $tr += $value->remaining;
		}

		$labels = substr($labels, 0, -1);
		$remain = substr($remain, 0, -1);
		$done = substr($done, 0, -1);

		$arr = array(
			"labels" => explode(',', "'',".$labels.",''"),
			"remain" => explode(',', "'',".$remain.",''"),
			"done" => explode(',', "'',".$done.",''"),
			"td" => $td,
			"tr" => $tr
		);

		echo json_encode($arr);
		exit();
	}

	public function add_user() {
		$this->form_validation->set_rules('email', 'Email ', 'required|xss_clean');
		$this->form_validation->set_rules('display', 'Display Name ', 'required|xss_clean|ucwords');
		$this->form_validation->set_rules('password', 'password ', 'required|xss_clean');

		if($this->form_validation->run()) {
			$user_data = array(
				"username" => $this->input->post('email'),
				"password" => md5($this->input->post('password')),
				"display_name" => $this->input->post('display'),
				"status" => 1,
				"date_created" => date('Y-m-d H:i:s', time())
			);

			if($this->User_Model->add_user($user_data) >= 1) {
				redirect(base_url().'app/?success=add_user');
			} else {
				redirect(base_url().'app/?failed=add_user');
			}
		} else {
			redirect(base_url().'app/?failed=add_user');
		}
	}
}

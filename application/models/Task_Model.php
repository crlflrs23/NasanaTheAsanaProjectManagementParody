<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task_Model extends CI_Model {

	function __construct(){
	parent::__construct();
	}

	function add_task($data) {
		$this->db->insert('nsn_tasks', $data);
		$check = $this->db->insert_id();
		return $check;
	}

	function add_section($data) {
		$this->db->insert('nsn_sections', $data);
		$check = $this->db->insert_id();
		return $check;
	}

	function check_section($section_name) {
		$this->db->select('COUNT(*) as count');
        $this->db->from('nsn_sections');
		$this->db->where('section_name', $section_name);
        $query = $this->db->get();
        return $query->row();
	}

	function get_section_data($section_name) {
		$this->db->select('*');
        $this->db->from('nsn_sections');
		$this->db->where('section_name', $section_name);
		$this->db->limit('1');
        $query = $this->db->get();
        return $query->row();
	}

	function get_section_data_id($section_id) {
		$this->db->select('*');
        $this->db->from('nsn_sections');
		$this->db->where('section_id', $section_id);
        $query = $this->db->get();
        return $query->row();
	}

	function get_all_sections() {
		return $this->db->get('nsn_sections')->result();
	}

	function get_all_task_data($section_id) {
		$this->db->where('section_id', $section_id);
		$this->db->order_by('task_priority', 'desc');
		return $this->db->get('nsn_tasks')->result();
	}

	function get_task_data($task_id){
		$this->db->where('task_id', $task_id);
		return $this->db->get('nsn_tasks')->row();
	}

	function update_task_status($id, $data) {
		$this->db->where('task_id', $id);
		$this->db->update('nsn_tasks', $data);
		return $this->db->affected_rows();
	}

	function get_user_task($user_id) {
		$this->db->where('task_assign', $user_id);
		return $this->db->get('nsn_tasks')->result();
	}

	function get_chart_data() {
		$this->db->select("DATE_FORMAT(a.date_created,'%m-%d-%Y') as date, (select count(*) from nsn_tasks as b where DATE_FORMAT(b.date_created,'%m-%d-%Y') = DATE_FORMAT(a.date_created,'%m-%d-%Y') AND b.task_status = '0') as remaining, (select count(*) from nsn_tasks as c where DATE_FORMAT(c.date_created,'%m-%d-%Y') = DATE_FORMAT(a.date_created,'%m-%d-%Y') AND c.task_status = '1') as done");
		$this->db->from('nsn_tasks as a');
		$this->db->group_by("DATE_FORMAT(a.date_created,'%m-%d-%Y')");
		$query = $this->db->get();
		return $query->result();
	}

	function update_task($data, $id) {
		$this->db->where('task_id', $id);
		$this->db->update('nsn_tasks', $data);
		$check = $this->db->affected_rows();
		return $check;
	}

	function delete_task($task_id) {
		$this->db->where('task_id', $task_id);
		$this->db->delete('nsn_tasks');
		$check = $this->db->affected_rows();
		return $check;
	}

	function delete_task_section($section_id) {
		$this->db->where_in('section_id', $section_id);
		$this->db->delete('nsn_tasks');
		$check = $this->db->affected_rows();
		return $check;
	}

	function delete_section($section_id) {
		$this->db->where('section_id', $section_id);
		$this->db->delete('nsn_sections');
		$check = $this->db->affected_rows();
		return $check;
	}

	function update_section($data, $id) {
		$this->db->where('section_id', $id);
		$this->db->update('nsn_sections', $data);
		$check = $this->db->affected_rows();
		return $check;
	}
}

<form action="<?php echo base_url() ?>app/edit_task_info/<?php echo $task->task_id; ?>" method="post">
    <span class="module-title">Edit Task</span><br/><br/>
    <center>
        <label for="task_title" class="input-label">Task Title</label>
        <span class="field"><input class="text input xxwide" type="text" name="task_title" placeholder="Task Title" value="<?php echo $task->task_title ?>"></span>
        <?php echo form_error('task_title', '<span class="frm-error">', '</span>'); ?>
        <br/><br/>

        <label for="task_description" class="input-label">Task Description</label>
        <span class="field"><textarea class="textarea input xxwide" name="task_description" style="font-size: 12px;" placeholder="Task Description"><?php echo $task->task_desc ?></textarea></span>
        <?php echo form_error('task_description', '<span class="frm-error">', '</span>'); ?>
        <br/><br/>

        <div class="row" style="width: 100%;">
            <div class="five columns">
                <label for="task_priority" class="input-label">Task Priority</label>
                <span class="field">
                    <select class="select input xxwide" name="task_priority">
                        <option value="#" disabled>Task Priority</option>
                        <option <?php echo ($task->task_priority == 1)? 'selected' : ''; ?> value="1">Low Priority</option>
                        <option <?php echo ($task->task_priority == 2)? 'selected' : ''; ?> value="2">Med Priority</option>
                        <option <?php echo ($task->task_priority == 3)? 'selected' : ''; ?> value="3">High Priority</option>
                    </select>
                </span><br/>
                <?php echo form_error('task_priority', '<span class="frm-error">', '</span>'); ?>
            </div>
            <div class="six columns">
                <label for="task_section" class="input-label">Task Section</label>
                <span class="field">
                    <input class="text input xxwide" type="text" name="task_section" placeholder="Task Section"  value="<?php echo $this->Task_Model->get_section_data_id($task->section_id)->section_name ?>" list="task_sections">
                    <datalist id="task_sections">
                        <?php
                            foreach ($sections as $key => $value) {
                                echo "<option value='". $value->section_name ."'>";
                            }
                        ?>
                    </datalist>
                </span><br/>
                <?php echo form_error('task_section', '<span class="frm-error">', '</span>'); ?>
            </div>
            <div class="five columns">
                <label for="task_assign" class="input-label">Assign to</label>
                <span class="field">
                    <input class="text input xxwide" type="text" name="task_assign" placeholder="Team Member" value="<?php echo $this->User_Model->get_user_by_id($task->task_assign)->username  ?>" list="team_member">
                    <datalist id="team_member">
                        <?php
                            foreach ($users as $key => $value) {
                                echo "<option value='". $value->username ."'>";
                            }
                        ?>
                    </datalist>
                </span><br/>
                <?php echo form_error('task_assign', '<span class="frm-error">', '</span>'); ?>
            </div>
        </div>
        <?php echo form_error('task_assign', '<span class="frm-error">', '</span>'); ?>
        <br/>
        <span><input type="submit" style="color: #fff;" class="btn-frm-submit" value="Update Task" /></span>
    </center>
</form>

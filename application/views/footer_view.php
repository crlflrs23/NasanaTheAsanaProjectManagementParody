
<footer>
    <div class="sixteen colgrids">
    </div>
</footer>
<!-- Grab Google CDN's jQuery, fall back to local if offline -->
<!-- 2.0 for modern browsers, 1.10 for .oldie -->
<script>
var oldieCheck = Boolean(document.getElementsByTagName('html')[0].className.match(/\soldie\s/g));
if(!oldieCheck) {
document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"><\/script>');
} else {
document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"><\/script>');
}
</script>
<script>
if(!window.jQuery) {
if(!oldieCheck) {
  document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');
} else {
  document.write('<script src="js/libs/jquery-1.10.1.min.js"><\/script>');
}
}
</script>
<script>
$(function() {
    $("#mobile-menu-trigger").click(function(){
        $("#main-mobile-menu").slideToggle("fast");
    });

    $(".menu-extend").click(function() {
        $(this).find('ul').slideToggle("fast");
    });
});
</script>

<!--
Include gumby.js followed by UI modules followed by gumby.init.js
Or concatenate and minify into a single file -->
<script gumby-touch="js/libs" src="<?php echo base_url() ?>js/libs/gumby.js"></script>
<script src="<?php echo base_url() ?>js/libs/ui/gumby.retina.js"></script>
<script src="<?php echo base_url() ?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?php echo base_url() ?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?php echo base_url() ?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?php echo base_url() ?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?php echo base_url() ?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?php echo base_url() ?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?php echo base_url() ?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?php echo base_url() ?>js/libs/ui/jquery.validation.js"></script>
<script src="<?php echo base_url() ?>js/libs/gumby.init.js"></script>

<!--
Google's recommended deferred loading of JS
gumby.min.js contains gumby.js, all UI modules and gumby.init.js
<script type="text/javascript">
function downloadJSAtOnload() {
var element = document.createElement("script");
element.src = "js/libs/gumby.min.js";
document.body.appendChild(element);
}
if (window.addEventListener)
window.addEventListener("load", downloadJSAtOnload, false);
else if (window.attachEvent)
window.attachEvent("onload", downloadJSAtOnload);
else window.onload = downloadJSAtOnload;
</script> -->

<script src="js/plugins.js"></script>
<script src="js/main.js"></script>

<!-- Change UA-XXXXX-X to be your site's ID -->
<!--<script>
window._gaq = [['_setAccount','UAXXXXXXXX1'],['_trackPageview'],['_trackPageLoadTime']];
Modernizr.load({
load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
});
</script>-->

<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
 chromium.org/developers/how-tos/chrome-frame-getting-started -->
<!--[if lt IE 7 ]>
<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
<![endif]-->

</body>
</html>

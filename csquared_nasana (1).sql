-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2016 at 03:12 PM
-- Server version: 5.6.26
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `csquared_nasana`
--

-- --------------------------------------------------------

--
-- Table structure for table `nsn_sections`
--

CREATE TABLE IF NOT EXISTS `nsn_sections` (
  `section_id` int(11) NOT NULL,
  `section_name` varchar(120) NOT NULL,
  `date_created` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `section_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nsn_tasks`
--

CREATE TABLE IF NOT EXISTS `nsn_tasks` (
  `task_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `task_title` varchar(120) NOT NULL,
  `task_desc` text NOT NULL,
  `task_assign` int(11) NOT NULL,
  `task_added` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `task_done` datetime NOT NULL,
  `task_priority` int(11) NOT NULL,
  `task_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nsn_users`
--

CREATE TABLE IF NOT EXISTS `nsn_users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(120) NOT NULL,
  `display_name` varchar(60) NOT NULL,
  `date_created` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nsn_users`
--

INSERT INTO `nsn_users` (`user_id`, `username`, `password`, `display_name`, `date_created`, `status`) VALUES
(1, 'p', '83878c91171338902e0fe0fb97a8c47a', 'Carlo', '2015-12-13 00:00:00', 1),
(3, 'roberto@nasana.com', 'roberto@nasana.com', 'roberto@nasana.com', '2015-12-18 17:14:11', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nsn_sections`
--
ALTER TABLE `nsn_sections`
  ADD PRIMARY KEY (`section_id`);

--
-- Indexes for table `nsn_tasks`
--
ALTER TABLE `nsn_tasks`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `nsn_users`
--
ALTER TABLE `nsn_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nsn_sections`
--
ALTER TABLE `nsn_sections`
  MODIFY `section_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nsn_tasks`
--
ALTER TABLE `nsn_tasks`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nsn_users`
--
ALTER TABLE `nsn_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
